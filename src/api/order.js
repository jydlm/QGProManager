import request from '@/utils/request'

export function getOrderList(data) {
  return request({
    url: '/product/productOrderList',
    method: 'post',
    data
  })
}

export function orderDelivery(data) {
  return request({
    url: '/product/orderDelivery',
    method: 'post',
    data
  })
}

export function addInvoice(data) {
  return request({
    url: '/product/addInvoice',
    method: 'post',
    data
  })
}




//无用
export function updateOrder(data) {
  return request({
    url: '/order/update',
    method: 'post',
    data
  })
}

export function getEndorsementList(data) {
  return request({
    url: '/endorsement/cmsGetList',
    method: 'post',
    data
  })
}

export function updateEndorsement(data) {
  return request({
    url: '/endorsement/update',
    method: 'post',
    data
  })
}

export function getCouponList(data) {
  return request({
    url: '/coupon/cmsSearch',
    method: 'post',
    data
  })
}

export function addCoupon(data) {
  return request({
    url: '/coupon/add',
    method: 'post',
    data
  })
}