import request from '@/utils/request'

export function getProductList(data) {
  return request({
    url: '/product/productManageList',
    method: 'post',
    data
  })
}

export function updateProduct(data) {
  return request({
    url: '/product/modProduct',
    method: 'post',
    data
  })
}

export function deleteProduct(data) {
  return request({
    url: '/product/delProduct',
    method: 'post',
    data
  })
}

export function addProduct(data) {
  return request({
    url: '/product/addProduct',
    method: 'post',
    data
  })
}