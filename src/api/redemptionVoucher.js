import request from '@/utils/request'

export function fetchRedemptionVoucher(data) {
  return request({
    url: '/code/selectCodeListByPage',
    method: 'post',
    data
  })
}

export function addRedemptionVoucher(data) {
  return request({
    url: 'code/add',
    method: 'post',
    data
  })
}

export function queryRedemptionDetail(data) {
  return request({
    url: 'code/selectCodeExchangeListByPage',
    method: 'post',
    data
  })
}

export function queryOrderDetail(data) {
  return request({
    url: 'code/selectCodeOrderListByPage',
    method: 'post',
    data
  })
}
