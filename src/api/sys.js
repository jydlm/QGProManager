import request from '@/utils/request'

export function getAdminList(data) {
  return request({
    url: 'admin/getQgAdminList',
    method: 'post',
    data
  })
}

export function addAdmin(data) {
  return request({
    url: '/admin/addQgAdmin',
    method: 'post',
    data
  })
}

export function deleteAdmin(data) {
  return request({
    url: '/admin/delQgAdmin',
    method: 'post',
    data
  })
}

export function updatePassw(data) {
  return request({
    url: '/admin/modPassword',
    method: 'post',
    data
  })
}

export function sendCode(data) {
  return request({
    url: '/verifycode/send',
    method: 'post',
    data
  })
}