import request from '@/utils/request'

export function getUserList(data) {
  return request({
    url: '/user/getUserList',
    method: 'post',
    data
  })
}

export function updateUser(data) {
  return request({
    url: '/user/update',
    method: 'post',
    data
  })
}

export function updateUserHouseCard(data) {
  return request({
    url: '/user/addUserHouseCard',
    method: 'post',
    data
  })
}

export function updateUserBankFlow(data) {
  return request({
    url: '/user/addUserBankRunningWater',
    method: 'post',
    data
  })
}

export function loginAdmin(data) {
  return request({
    url: '/qg/admin/login',
    method: 'post',
    data
  })
}

export function getUserById(data) {
  return request({
    url: '/user/get',
    method: 'post',
    data
  })
}
