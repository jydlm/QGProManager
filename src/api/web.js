import request from '@/utils/request'

// banner
export function getBannerList(data) {
  return request({
    url: '/config/bannerList',
    method: 'post',
    data
  })
}

export function updateBanner(data) {
  return request({
    url: '/config/modBanner',
    method: 'post',
    data
  })
}

export function deleteBanner(data) {
  return request({
    url: '/config/delBanner',
    method: 'post',
    data
  })
}

export function addBanner(data) {
  return request({
    url: '/config/addBanner',
    method: 'post',
    data
  })
}

// video
export function getVideoList(data) {
  return request({
    url: '/config/videoList',
    method: 'post',
    data
  })
}

export function updateVideo(data) {
  return request({
    url: '/config/modVideo',
    method: 'post',
    data
  })
}

export function deleteVideo(data) {
  return request({
    url: '/config/delVideo',
    method: 'post',
    data
  })
}

export function addVideo(data) {
  return request({
    url: '/config/addVideo',
    method: 'post',
    data
  })
}

// 文化园地
export function getCultureGardenList(data) {
  return request({
    url: '/config/culturalGardenList',
    method: 'post',
    data
  })
}

export function updateCultureGarden(data) {
  return request({
    url: '/config/modCulturalGarden',
    method: 'post',
    data
  })
}

export function deleteCultureGarden(data) {
  return request({
    url: '/config/delCulturalGarden',
    method: 'post',
    data
  })
}

export function addCultureGarden(data) {
  return request({
    url: '/config/addCulturalGarden',
    method: 'post',
    data
  })
}

//健康贴士
export function getHealthTipList(data) {
  return request({
    url: '/config/healthTipsList',
    method: 'post',
    data
  })
}

export function updateHealthTip(data) {
  return request({
    url: '/config/modHealthTips',
    method: 'post',
    data
  })
}

export function deleteHealthTip(data) {
  return request({
    url: '/config/delHealthTips',
    method: 'post',
    data
  })
}

export function addHealthTip(data) {
  return request({
    url: '/config/addHealthTips',
    method: 'post',
    data
  })
}
//品牌故事——关于我们
export function getBrandStoryList(data) {
  return request({
    url: '/config/brandStoryList',
    method: 'post',
    data
  })
}

export function updateBrandStory(data) {
  return request({
    url: '/config/modBrandStory',
    method: 'post',
    data
  })
}

export function deleteBrandStory(data) {
  return request({
    url: '/config/delBrandStory',
    method: 'post',
    data
  })
}
export function addBrandStory(data) {
  return request({
    url: '/config/addBrandStory',
    method: 'post',
    data
  })
}
//信息详情——关于我们
export function getUsDetailList(data) {
  return request({
    url: '/config/usDetailList',
    method: 'post',
    data
  })
}

export function updateUsDetail(data) {
  return request({
    url: '/config/modUsDetail',
    method: 'post',
    data
  })
}

export function deleteUsDetail(data) {
  return request({
    url: '/config/delUsDetail',
    method: 'post',
    data
  })
}

export function addUsDetail(data) {
  return request({
    url: '/config/addUsDetail',
    method: 'post',
    data
  })
}
//联系我们
export function getCompanyList(data) {
  return request({
    url: '/config/companyList',
    method: 'post',
    data
  })
}

export function addCompany(data) {
  return request({
    url: '/config/addCompany',
    method: 'post',
    data
  })
}
//合作伙伴——关于我们
export function getPartnersList(data) {
  return request({
    url: '/config/partnersList',
    method: 'post',
    data
  })
}

export function updatePartners(data) {
  return request({
    url: '/config/modPartners',
    method: 'post',
    data
  })
}

export function deletePartners(data) {
  return request({
    url: '/config/delPartners',
    method: 'post',
    data
  })
}

export function addPartners(data) {
  return request({
    url: '/config/addPartners',
    method: 'post',
    data
  })
}
//常见问题
export function getFaqList(data) {
  return request({
    url: '/config/faqList',
    method: 'post',
    data
  })
}

export function updateFaq(data) {
  return request({
    url: '/config/modFaq',
    method: 'post',
    data
  })
}

export function deleteFaq(data) {
  return request({
    url: '/config/delFaq',
    method: 'post',
    data
  })
}

export function addFaq(data) {
  return request({
    url: '/config/addFaq',
    method: 'post',
    data
  })
}