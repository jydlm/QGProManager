import Vue from 'vue'
import SvgIcon from '@/components/SvgIcon'// svg组件

// register globally
Vue.component('svg-icon', SvgIcon)

//（创建了）一个包含了 svg 文件夹（不包含子目录）下面的、所有文件名以 `.svg` 结尾的、能被 require 请求到的文件的上下文。
const req = require.context('./svg', false, /\.svg$/)
const requireAll = requireContext => requireContext.keys().map(requireContext)
requireAll(req)
