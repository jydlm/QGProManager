import Vue from 'vue'
import Router from 'vue-router'
import Layout from '@/views/layout/Layout'
import webRouter from './modules/web'
import productRouter from './modules/product'


import carRouter from './modules/car'
import userRouter from './modules/user'
import orderRouter from './modules/order'
import bussinessRouter from './modules/bussiness'
import peccancyRouter from './modules/peccancy'
import couponRouter from './modules/coupon'
import {routerAll,routerLimit} from './modules/sys'
import supercouponRouter from './modules/supercoupon'

Vue.use(Router)

// 无需判断权限的页面
export const constantRouterMap = [

  // {
  //   path: '/',
  //   component: Layout,
  //   redirect: 'dashboard'
  // },

  {
    path: '/',
    component: Layout,
    redirect: 'dashboard',
    children: [
      {
        path: 'dashboard',
        component: () => import('@/views/dashboard/index'),
        name: 'Dashboard',
        meta: { title: ' 首页', icon: 'dashboard', noCache: true }
      }
    ]
  },

  {
    path: '/404',
    component: () => import('@/views/errorPage/404'),
    hidden: true
  },

  {
    path: '/userLogin',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/userLogout',
    component: () => import('@/views/logout/index'),
    hidden: true
  }

  // {
  //   path: '/icon',
  //   component: Layout,
  //   children: [
  //     {
  //       path: 'index',
  //       component: () => import('@/views/svg-icons/index'),
  //       name: 'Icons',
  //       meta: { title: 'icons', icon: 'icon', noCache: true }
  //     }
  //   ]
  // }

]

export default new Router({
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRouterMap
})

// 需要权限控制的页面，
// 超级管理员
export const asyncRouterMap = [
  webRouter,
  productRouter,
  orderRouter,
  userRouter,
  routerAll,



  // carRouter,
  // bussinessRouter,
  // peccancyRouter,
  // supercouponRouter,
  { path: '*', redirect: '/404', hidden: true }
]

// 普通管理页
export const asyncRouterMap1 = [
  webRouter,
  productRouter,
  orderRouter,
  userRouter,
  routerLimit,


  // carRouter,
  // bussinessRouter,
  // peccancyRouter,
  // couponRouter,
  { path: '*', redirect: '/404', hidden: true }
]
