import Layout from '@/views/layout/Layout'

const tableRouter = {
  path: '/product',
  component: Layout,
  redirect: '/product/product-list',
  name: 'product',
  meta: {
    title: '产品管理',
    icon: 'list'
  },
  children: [
    {
      path: 'product-list',
      component: () => import('@/views/product/productList'),
      name: 'productList',
      meta: { title: '产品列表' }
    }
  ]
}
export default tableRouter
