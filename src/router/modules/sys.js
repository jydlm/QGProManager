import Layout from '@/views/layout/Layout'

export const routerAll = {
  path: '/sys',
  component: Layout,
  redirect: '/sys/list-msg',
  name: 'Sys',
  meta: {
    title: '系统管理',
    icon: 'example'
  },
  children: [
    // {
    //   path: 'add-msg',
    //   component: () => import('@/views/sys/addMsg'),
    //   name: 'addMsg',
    //   meta: { title: '发布消息' }
    // },
    {
      path: 'list-msg',
      component: () => import('@/views/sys/adminList'),
      name: 'listMsg',
      meta: { title: '管理员列表' }
    },
    {
      path: 'mod-pwd',
      component: () => import('@/views/sys/modPwd'),
      name: 'carList',
      meta: { title: '密码修改' }
    }
  ]
}
export const routerLimit = {
  path: '/sys',
  component: Layout,
  // redirect: '/sys/list-msg',
  name: 'Sys',
  meta: {
    title: '系统管理',
    icon: 'example'
  },
  children: [
    {
      path: 'mod-pwd',
      component: () => import('@/views/sys/modPwd'),
      name: 'carList',
      meta: { title: '密码修改' }
    }
  ]
}
// export default {routerAll,routerLimit}
// export default
