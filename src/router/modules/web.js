import Layout from '@/views/layout/Layout'

const tableRouter = {
  path: '/web',
  component: Layout,
  redirect: '/web/banner-config',
  name: 'web',
  meta: {
    title: '官网配置',
    icon: 'component'
  },
  children: [
    {
      path: 'banner-config',
      component: () => import('@/views/web/bannerList'),
      name: 'bannerConfig',
      meta: { title: '首页Banner' }
    },
    {
      path: 'video-config',
      component: () => import('@/views/web/videoList'),
      name: 'videoList',
      meta: { title: '首页Video' }
    },
    {
      path: 'cultureGarden-config',
      component: () => import('@/views/web/cultureGardenConfig'),
      name: 'cultureGardenConfig',
      meta: { title: '文化园地' }
    },
    {
      path: 'healthTip-config',
      component: () => import('@/views/web/healthTipConfig'),
      name: 'healthTipConfig',
      meta: { title: '健康贴士' }
    },
    {
      path: 'details-config',
      component: () => import('@/views/web/aboutUsConfig/detailsConfig'),
      name: 'detailsConfig',
      meta: { title: '公司简介' }
    },
    {
      path: 'brandStory-config',
      component: () => import('@/views/web/aboutUsConfig/brandStoryConfig'),
      name: 'brandStoryConfig',
      meta: { title: '品牌故事' }
    },
    {
      path: 'partners-config',
      component: () => import('@/views/web/aboutUsConfig/partnersConfig'),
      name: 'partnersConfig',
      meta: { title: '合作伙伴' }
    },
    {
      path: 'contactUs-config',
      component: () => import('@/views/web/contactUsConfig'),
      name: 'contactUsConfig',
      meta: { title: '联系我们' }
    },
    {
      path: 'faq-config',
      component: () => import('@/views/web/faqConfig'),
      name: 'faqConfig',
      meta: { title: '常见问题' }
    },
  ]
}
export default tableRouter
