import Cookies from 'js-cookie'

const TokenKey = 'jf_role'

export function getToken() {
  const userId = Cookies.get('userId')
  if (userId === '7') {
    return 'admin'
  } else {
    return 'editor'
  }
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}
